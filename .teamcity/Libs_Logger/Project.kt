package Libs_Logger

import jetbrains.buildServer.configs.kotlin.v2018_1.BuildType
import jetbrains.buildServer.configs.kotlin.v2018_1.Project
import jetbrains.buildServer.configs.kotlin.v2018_1.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_1.triggers.vcs

object Libs_Logger_Build_BuildType : BuildType() {
    init {
        name = "build"
        vcs {
            root(_Self.Testdata3_Repo,  "+:libs/logger")
            cleanCheckout = true
        }
        triggers {
            vcs {
                triggerRules = "+:libs/logger"
                watchChangesInDependencies = true
            }
        }
        steps {
            script {
                name = "script"
                workingDir = "libs/logger"
                scriptContent = """
                        |#@IgnoreInspection BashAddShebang
                        |counter=0
                        |
                        |function _block_open {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockOpened name='${"$"}{id}']"
                        |}
                        |
                        |function _block_closed {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockClosed name='${"$"}{id}']"
                        |  counter=${"$"}((counter+1))
                        |}
                        |
                        |function _try64 {
                        |  _block_open
                        |
                        |  local script=${"$"}(echo ${"$"}1 | base64 --decode)
                        |
                        |  echo "${"$"} ${"$"}{script}"
                        |  eval "${"$"}{script}"
                        |  local code=${"$"}?
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    echo "##teamcity[message text='the command exits with code ${"$"}{code}' status='ERROR']"
                        |  fi
                        |
                        |  _block_closed
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    exit ${"$"}{code}
                        |  fi
                        |}
                        |
                        |## echo logger
                        |_try64 ZWNobyBsb2dnZXI=
                        |""".trimMargin()
            }
        }
    }
}

object Project : Project() {
    init {
        id("Libs_Logger")
        name = "Libs_Logger"
        buildType(Libs_Logger_Build_BuildType)
        buildTypesOrder = arrayListOf(Libs_Logger_Build_BuildType)
    }
}
