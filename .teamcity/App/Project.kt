package App

import jetbrains.buildServer.configs.kotlin.v2018_1.BuildType
import jetbrains.buildServer.configs.kotlin.v2018_1.BuildTypeSettings
import jetbrains.buildServer.configs.kotlin.v2018_1.Project
import jetbrains.buildServer.configs.kotlin.v2018_1.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_1.triggers.vcs

object App_TestsBuild_BuildType : BuildType() {
    init {
        name = "tests::build"
        artifactRules = "app/target/context.tgz"
        vcs {
            root(_Self.Testdata3_Repo,  "+:app", "+:libs/core", "+:libs/logger")
            cleanCheckout = true
        }
        steps {
            script {
                name = "script"
                workingDir = "app"
                scriptContent = """
                        |#@IgnoreInspection BashAddShebang
                        |counter=0
                        |
                        |function _block_open {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockOpened name='${"$"}{id}']"
                        |}
                        |
                        |function _block_closed {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockClosed name='${"$"}{id}']"
                        |  counter=${"$"}((counter+1))
                        |}
                        |
                        |function _try64 {
                        |  _block_open
                        |
                        |  local script=${"$"}(echo ${"$"}1 | base64 --decode)
                        |
                        |  echo "${"$"} ${"$"}{script}"
                        |  eval "${"$"}{script}"
                        |  local code=${"$"}?
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    echo "##teamcity[message text='the command exits with code ${"$"}{code}' status='ERROR']"
                        |  fi
                        |
                        |  _block_closed
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    exit ${"$"}{code}
                        |  fi
                        |}
                        |
                        |## echo build > file
                        |_try64 ZWNobyBidWlsZCA+IGZpbGU=
                        |
                        |## mkdir -p target
                        |_try64 bWtkaXIgLXAgdGFyZ2V0
                        |
                        |## tar -cvzf target/context.tgz file
                        |_try64 dGFyIC1jdnpmIHRhcmdldC9jb250ZXh0LnRneiBmaWxl
                        |""".trimMargin()
            }
        }
    }
}

object App_TestsBench_BuildType : BuildType() {
    init {
        name = "tests::bench"
        vcs {
            root(_Self.Testdata3_Repo,  "+:app", "+:libs/core", "+:libs/logger")
            cleanCheckout = true
        }
        steps {
            script {
                name = "script"
                workingDir = "app"
                scriptContent = """
                        |#@IgnoreInspection BashAddShebang
                        |counter=0
                        |
                        |function _block_open {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockOpened name='${"$"}{id}']"
                        |}
                        |
                        |function _block_closed {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockClosed name='${"$"}{id}']"
                        |  counter=${"$"}((counter+1))
                        |}
                        |
                        |function _try64 {
                        |  _block_open
                        |
                        |  local script=${"$"}(echo ${"$"}1 | base64 --decode)
                        |
                        |  echo "${"$"} ${"$"}{script}"
                        |  eval "${"$"}{script}"
                        |  local code=${"$"}?
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    echo "##teamcity[message text='the command exits with code ${"$"}{code}' status='ERROR']"
                        |  fi
                        |
                        |  _block_closed
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    exit ${"$"}{code}
                        |  fi
                        |}
                        |
                        |## echo bench
                        |_try64 ZWNobyBiZW5jaA==
                        |""".trimMargin()
            }
        }
    }
}

object App_DockerBuild_BuildType : BuildType() {
    init {
        name = "docker::build"
        vcs {
            root(_Self.Testdata3_Repo,  "+:app", "+:libs/core", "+:libs/logger")
            cleanCheckout = true
        }
        dependencies {
            dependency(App_TestsBuild_BuildType) {
                snapshot {
                    onDependencyFailure = jetbrains.buildServer.configs.kotlin.v2018_1.FailureAction.CANCEL
                    onDependencyCancel = jetbrains.buildServer.configs.kotlin.v2018_1.FailureAction.CANCEL
                }
                artifacts {
                    cleanDestination = true
                    artifactRules = "context.tgz => app/target/"
                }
            }
            dependency(App_TestsBench_BuildType) {
                snapshot {
                    onDependencyFailure = jetbrains.buildServer.configs.kotlin.v2018_1.FailureAction.CANCEL
                    onDependencyCancel = jetbrains.buildServer.configs.kotlin.v2018_1.FailureAction.CANCEL
                }
            }
        }
        steps {
            script {
                name = "script"
                workingDir = "app"
                scriptContent = """
                        |#@IgnoreInspection BashAddShebang
                        |counter=0
                        |
                        |function _block_open {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockOpened name='${"$"}{id}']"
                        |}
                        |
                        |function _block_closed {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockClosed name='${"$"}{id}']"
                        |  counter=${"$"}((counter+1))
                        |}
                        |
                        |function _try64 {
                        |  _block_open
                        |
                        |  local script=${"$"}(echo ${"$"}1 | base64 --decode)
                        |
                        |  echo "${"$"} ${"$"}{script}"
                        |  eval "${"$"}{script}"
                        |  local code=${"$"}?
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    echo "##teamcity[message text='the command exits with code ${"$"}{code}' status='ERROR']"
                        |  fi
                        |
                        |  _block_closed
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    exit ${"$"}{code}
                        |  fi
                        |}
                        |
                        |## echo docker
                        |_try64 ZWNobyBkb2NrZXI=
                        |
                        |## find . 
                        |_try64 ZmluZCAuIA==
                        |""".trimMargin()
            }
        }
    }
}

object App_DeployProduction_BuildType : BuildType() {
    init {
        name = "deploy::production"
        type = BuildTypeSettings.Type.DEPLOYMENT
        vcs {
            root(_Self.Testdata3_Repo,  "+:app", "+:libs/core", "+:libs/logger")
            cleanCheckout = true
        }
        triggers {
            vcs {
                triggerRules = """
                        |+:app
                        |+:libs/core
                        |+:libs/logger
                        """.trimMargin()
                watchChangesInDependencies = true
            }
        }
        dependencies {
            dependency(App_DockerBuild_BuildType) {
                snapshot {
                    onDependencyFailure = jetbrains.buildServer.configs.kotlin.v2018_1.FailureAction.CANCEL
                    onDependencyCancel = jetbrains.buildServer.configs.kotlin.v2018_1.FailureAction.CANCEL
                }
            }
        }
        steps {
            script {
                name = "script"
                workingDir = "app"
                scriptContent = """
                        |#@IgnoreInspection BashAddShebang
                        |counter=0
                        |
                        |function _block_open {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockOpened name='${"$"}{id}']"
                        |}
                        |
                        |function _block_closed {
                        |  local id="script.${"$"}{counter}"
                        |  echo "##teamcity[blockClosed name='${"$"}{id}']"
                        |  counter=${"$"}((counter+1))
                        |}
                        |
                        |function _try64 {
                        |  _block_open
                        |
                        |  local script=${"$"}(echo ${"$"}1 | base64 --decode)
                        |
                        |  echo "${"$"} ${"$"}{script}"
                        |  eval "${"$"}{script}"
                        |  local code=${"$"}?
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    echo "##teamcity[message text='the command exits with code ${"$"}{code}' status='ERROR']"
                        |  fi
                        |
                        |  _block_closed
                        |
                        |  if [ ${"$"}{code} -ne 0 ]; then
                        |    exit ${"$"}{code}
                        |  fi
                        |}
                        |
                        |## echo staging
                        |_try64 ZWNobyBzdGFnaW5n
                        |""".trimMargin()
            }
        }
    }
}

object Project : Project() {
    init {
        id("App")
        name = "App"
        buildType(App_TestsBuild_BuildType)
        buildType(App_TestsBench_BuildType)
        buildType(App_DockerBuild_BuildType)
        buildType(App_DeployProduction_BuildType)
        buildTypesOrder = arrayListOf(App_TestsBuild_BuildType, App_TestsBench_BuildType, App_DockerBuild_BuildType, App_DeployProduction_BuildType)
    }
}
