package _Self

import jetbrains.buildServer.configs.kotlin.v2018_1.Project
import jetbrains.buildServer.configs.kotlin.v2018_1.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2018_1.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2018_1.vcs.GitVcsRoot

object Testdata3_Repo : GitVcsRoot() {
    init {
        name = "testdata3"
        url = "https://gitlab.com/dmexe/test.git"
        pushUrl = "https://gitlab.com/dmexe/test.git"
        branchSpec = """
                |+:refs/heads/(master)
                |+:refs/pull/(*/merge)
                """.trimMargin()
    }
}

object Project : Project() {
    init {
        description = "Contains all other projects"
        vcsRoot(Testdata3_Repo)
        subProject(App.Project)
        subProject(Libs_Core.Project)
        subProject(Libs_Logger.Project)
    }
}
